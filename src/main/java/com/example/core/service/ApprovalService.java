package com.example.core.service;

import com.example.core.model.ApprovalResponse;
import com.example.core.model.CreditCondition;

public interface ApprovalService {

    ApprovalResponse checkApprovment(Long userId, String creditConditionId, Float amount);
}
