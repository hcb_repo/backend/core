package com.example.core.service;

public interface QrService {

    byte[] generateQr(Long userId, String transactionId);
}
