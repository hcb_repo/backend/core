package com.example.core.service;

public interface CreditService {

    boolean makePayment(Long userId, String transactionId);
}
