package com.example.core.service;

public interface TextService {

    String extractArticul(String source);
    Float extractAmount(String source);
}
