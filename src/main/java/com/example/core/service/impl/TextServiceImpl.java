package com.example.core.service.impl;

import com.example.core.service.TextService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TextServiceImpl implements TextService {

    @Override
    public String extractArticul(String source) {
        String[] stringList = source.split("\n");
        for (String s : stringList) {
            if (s.toUpperCase(Locale.ROOT).contains("АРТИКУЛ")) return getNumberFromString(s);
        }


        return "";
    }

    private String getNumberFromString(String s){
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(s);
        if(m.find()) {
            return (m.group());
        }
        return "0";
    }



    @Override
    public Float extractAmount(String source) {
        String[] stringList = source.split("\n");
        String number;
        for (String s : stringList) {
            if (s.toUpperCase(Locale.ROOT).contains("ЦЕНА")) {
                number = getNumberFromString(s);
                Pattern p = Pattern.compile("([0-9]+[.][0-9]+)");
                Matcher m = p.matcher(s);
                if (m.find()) {
                    number = m.group();
                }

                return Float.parseFloat(number);

            }
        }

        return 0.0F;
    }
}
