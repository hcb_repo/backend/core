package com.example.core.service.mock;

import com.example.core.service.TextService;
import org.springframework.stereotype.Service;

@Service
public class MockTextServiceImpl implements TextService {
    @Override
    public String extractArticul(String source) {
        return "15500155";
    }

    @Override
    public Float extractAmount(String source) {
        return 500000.0F;
    }
}
