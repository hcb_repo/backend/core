package com.example.core.service.mock;

import com.example.core.model.ApprovalResponse;
import com.example.core.model.CreditCondition;
import com.example.core.service.ApprovalService;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class MockApprovalServiceImpl implements ApprovalService {


    @Override
    public ApprovalResponse checkApprovment(Long userId, String creditConditionId, Float amount) {
        Random random = new Random();
        ApprovalResponse response = new ApprovalResponse();
        response.setApproved(random.nextBoolean());
        response.setTransactionId(String.valueOf(random.nextLong()));
        return response;
    }
}
