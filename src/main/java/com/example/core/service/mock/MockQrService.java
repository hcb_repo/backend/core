package com.example.core.service.mock;

import com.example.core.service.QrService;
import org.springframework.stereotype.Service;

@Service
public class MockQrService implements QrService {


    @Override
    public byte[] generateQr(Long userId, String transactionId) {
        return new byte[0];
    }
}
