package com.example.core.service.mock;

import com.example.core.model.Product;
import com.example.core.service.ProductService;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

@Service
public class MockProductService implements ProductService {

    @Override
    public Product findByArticulId(String articulId) {
        byte[] data = null;
        try {
            File file = new File("classpath:mock/macBook.jpg");
            BufferedImage bImage = ImageIO.read(new File("sample.jpg"));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(bImage, "jpg", bos);
            data = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Product product = new Product();
        product.setArticulId("15500155");
        product.setDescription("MacBook");
        product.setAmount(500000.0F);
        product.setImg(data);
        return null;
    }
}
