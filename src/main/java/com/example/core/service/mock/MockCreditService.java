package com.example.core.service.mock;

import com.example.core.service.CreditService;
import org.springframework.stereotype.Service;

@Service
public class MockCreditService implements CreditService {

    @Override
    public boolean makePayment(Long userId, String transactionId) {
        return true;
    }
}
