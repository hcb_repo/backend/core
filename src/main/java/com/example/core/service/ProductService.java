package com.example.core.service;

import com.example.core.model.Product;

public interface ProductService {

    Product findByArticulId(String articulId);
}
