package com.example.core.controller;

import com.example.core.model.ApprovalResponse;
import com.example.core.model.CreditCondition;
import com.example.core.model.Product;
import com.example.core.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class Controller {

    @Qualifier("textServiceImpl")
    private final TextService textService;
    private final ProductService productService;
    private final ApprovalService approvalService;
    private final QrService qrService;
    private final CreditService creditService;

    @PostMapping("/scan-img")
    public ResponseEntity<Product> getCreditInfo(@RequestParam("file") MultipartFile file,
                                                 @RequestParam Long userId){


        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
        parts.add("Content-Type", "image/jpeg");
        parts.add("file", file.getResource());
        String serverUrl = "http://localhost:5000/scan-img";


        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(parts);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> text = restTemplate.exchange(serverUrl, HttpMethod.POST, request, String.class);
        String articul = textService.extractArticul(text.getBody());
        Float amount = textService.extractAmount(text.getBody());

        Product product = productService.findByArticulId(articul);
        return ResponseEntity.ok(product);
    }


    @GetMapping("/credit-conditions")
    public ResponseEntity<List<CreditCondition>> getCreditConditions(){
        ArrayList<CreditCondition> result = new ArrayList<>();
        result.add(new CreditCondition(3, 0.0F));
        result.add(new CreditCondition(6, 0.0F));
        result.add(new CreditCondition(12, 0.0F));
        return ResponseEntity.ok(result);
    }


    @PostMapping("/request-credit")
    public ResponseEntity<byte[]> requestCredit(@RequestBody List<Product> products,
                                               @RequestParam Long userId,
                                               @RequestParam String creditConditionId){
        Float sum = (float) products.stream().mapToDouble(Product::getAmount).sum();
        ApprovalResponse response = approvalService.checkApprovment(userId,creditConditionId, sum);
        if (response.getApproved()){
            return ResponseEntity.ok(qrService.generateQr(userId, response.getTransactionId()));
        }
        return ResponseEntity.badRequest().body(null);
    }


    @PostMapping("/approve-credit")
    public ResponseEntity<?> approveCredit(@RequestParam Long userId, @RequestParam String transactionId){
        if (creditService.makePayment(userId, transactionId)){
            notifyUser(userId);
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.badRequest().body(null);
    }

    private void notifyUser(Long userId){
        //TODO
    }

}
