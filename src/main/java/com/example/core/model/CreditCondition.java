package com.example.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class CreditCondition {

    private String id;
    private Integer mountCount;
    private Float percent;

    public CreditCondition(Integer mountCount, Float percent){
        this.mountCount = mountCount;
        this.percent = percent;
    }
}
