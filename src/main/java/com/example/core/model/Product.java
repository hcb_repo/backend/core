package com.example.core.model;

import lombok.Data;

@Data
public class Product {
    private String articulId;
    private Float amount;
    private String description;
    private byte[] img;
}
