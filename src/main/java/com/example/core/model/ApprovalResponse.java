package com.example.core.model;

import lombok.Data;

@Data
public class ApprovalResponse {

    private Boolean approved;
    private String transactionId;
}
